#include "m2.h"
#include <stdio.h>

instr::instr()
    {
       // opcode="Hello world";
    }

instr::instr(string str_instr)
        {
            int i=0;

            char * tstr=strtok((char *)str_instr.c_str(),"       = \n");
            in_addr=tstr;

            cout<<"\ninstr id = "<<tstr<<" fetch enter time = "<<(clk-1)<<endl;
            sr_ct=0;
            d_ct=0;
            for(i=0;i<nsrc;i++)
            {
                s[i].ds_type="\0";
                s[i].name="\0";
                s[i].ref='\0';
                sflg[i]=false;
            }
            for(i=0;i<ndest;i++)
            {
                d[i].ds_type="\0";
                d[i].name="\0";
                d[i].ref='\0';
                dflg[i]=false;
            }

            tstr=strtok(NULL,"       = \n");
            opcode=tstr;


            while(1)
                {

                       tstr=strtok(NULL,"       = \n");
                       if(tstr!=NULL)
                       {

                            if(!strcmp("SOURCE",tstr))
                            {

                                tstr=strtok(NULL," \n");
                                s[sr_ct].name=tstr;
                                s[sr_ct].ds_type="SOURCE";
                                if(tstr[0]=='r')
                                s[sr_ct].ref='R';
                                else
                                {
                                s[sr_ct].ref='M';
                                    if(!strcmp(opcode.c_str(),"LEA"))
                                    {
                                        rem_clk+=10;
                                    }
                                    if(!strcmp(opcode.c_str(),"CALL_NEAR"))
                                    {
                                        opcode="ADD";
                                    }
                                }
                                sr_ct++;

                            }
                            else if(!strcmp("DEST",tstr))
                            {

                                tstr=strtok(NULL," \n");
                                d[d_ct].name=tstr;
                                d[d_ct].ds_type="DEST";
                                if(tstr[0]=='r')
                                d[d_ct].ref='R';
                                else
                                d[d_ct].ref='M';
                                d_ct++;
                            }
                       }
                       else
                       break;
                }

        }

 void instr::print_instr()
        {
            cout<<in_addr<<endl;
            cout<<opcode<<endl;
            cout<<" "<<s[0].ds_type.c_str()<<" "<<s[0].name.c_str()<<endl;
            cout<<" "<<s[1].ds_type.c_str()<<" "<<s[1].name.c_str()<<endl;
            cout<<" "<<s[2].ds_type.c_str()<<" "<<s[2].name.c_str()<<endl;
            cout<<" "<<d[0].ds_type.c_str()<<" "<<d[0].name.c_str()<<endl;
            cout<<" "<<d[1].ds_type.c_str()<<" "<<d[1].name.c_str()<<endl;
            cout<<" "<<d[2].ds_type.c_str()<<" "<<d[2].name.c_str()<<endl;
        }

void read_reg_opr(instr itr)
{
	int i=0;
     //reading operands from register file..done only for source operands
	for(i=0;i<itr.sr_ct;i++)
	{
		  if(itr.s[i].name!="\0")
			{
					if(itr.s[i].ref=='R')
					{
						int map_size=reg_file.size();
						struct register_entry temp = reg_file[itr.s[i].name];
						if(map_size < reg_file.size())
						{
						    temp.instr_tag="  ---hello world register--- ";
						    temp.state=true;
						    reg_file[itr.s[i].name]=temp;
						    itr.sflg[i]=true;
						}
						else
						{
						    if(temp.state==true)
						    itr.sflg[i]=true;
						    else
						    itr.s[i].name=temp.instr_tag;
						    itr.sflg[i]=false;
						}

					}

			}
	}

}

void decode1()
{

        string str_inst;

        if((ifq_size>0) &&(idq12_size<idq12_size_max))
            {

                str_inst=ifq.front();
                ifq.pop();

                instr i1=instr(str_inst);

                i1.print_instr();
                cout<<"In decode 1 clk =  "<<clk<<" and instr id = "<<i1.in_addr<<endl;
                ifq_size--;

                if(idq12_size<idq12_size_max)
                {
                        idq12.push(i1);
                        idq12_size++;
                        //cout<<"\nidq size = "<<idq12_size<<endl;
                }

            }

}

void decode2()
{
    if(!idq12.empty())
    {
        instr i1=idq12.front();
        idq12.pop();
        idq12_size--;
        read_reg_opr(i1);
        idq.push(i1);
    }
}

void print_register_file()
{
    for( map<string,register_entry>::iterator ii=reg_file.begin(); ii!=reg_file.end(); ++ii)
       {
           cout << (*ii).first << " : " << (*ii).second.instr_tag << endl;
       }

}

void print_rob()
{
	cout<<"rob size = "<<rob.size()<<endl;
	cout<<"rob max size = "<<rob_sz_max<<endl;
    for( map<string,rob_entry>::iterator ii=rob.begin(); ii!=rob.end(); ++ii)
       {
		   //cout<<"rob";
		   int k=0;
           cout << (*ii).first << " : ";
           (*ii).second.inr.print_instr();
            //struct rob_entry re=(*ii).second;
           cout<<" "<<(*ii).second.ready<<" "<<endl;
       }

}

void fetch_instr()
{

 if(!ipfile.eof()&& (allow_fetch==true))
    {
        getline(ipfile,line);
        if(strstr(line.c_str(),"$$$")==NULL)
        {
        ifq.push(line);               //push instruction onto instruction fetch queue
        ifq_size++;
        cout<< "in fetch ifq_size = "<<ifq_size<<endl;
        }
        else
        {
        cout<<"END OF FILE"<<endl;
        ipfile.close();
        endflag=1;
        }
    }

}

string addtorob(instr inr)
{

		if(rob.size()<rob_sz_max)
		{
			string id=convertInt(last_index);
			//cout<<"in add to rob id = "<<id<<endl;
			last_index++;
			struct rob_entry r1;
			r1.ready=false;
			r1.inr=inr;
			rob[id]=r1;
			return id;
		}
		else
		{
			return "---";
		}


}
bool rob_avail()
{
	if(rob.size()<rob_sz_max)
	return true;
    else
	return false;
}
void perform_reg_renaming(r_slot rs)
{

	int op_ct=0;    //source operand counter
	int j=0;
	int k=0;

		map<string,rob_entry>::iterator rob_itr=rob.begin();
		//rob_entry cur_itr=rob[convertInt(last_index-1)];                      //pointer to rob-slot of the current instruction;
        //cout<<"\nin reg rnaming with last index = "<<last_index<<endl;
		for(k=first_index;k<last_index-1;k++)       //don't check last entry because it is the instruction that has currently received it's rob slot
		{
		   // cout<<"A*"<<endl;
		    if(rob_itr==rob.end())
		    break;
				rob_entry temp=(*rob_itr).second;
				instr ti=temp.inr;
	//			  cout<<"A"<<endl;
				for(j=0;j<ti.d_ct;j++)
				{
		//		  cout<<"B"<<endl;
					for(op_ct=0;op_ct<rs.inr.sr_ct;op_ct++)
					{
			//		    cout<<"C"<<endl;
						if(!strcmp(rs.inr.s[op_ct].name.c_str(),ti.d[j].name.c_str()))
						{
				//		    cout<<"D"<<endl;
						    rs.reg_rename[op_ct].name=(*rob_itr).first;
						    rs.reg_rename[op_ct].flag=true;
						    cout<<"Entered for register "<<ti.d[j].name.c_str()<<endl;
						    cout<<"matched rob entry is "<<(*rob_itr).first<<" matched name is "<<rs.reg_rename[op_ct].name<<endl;

						}
					}
				}
            if(rob_itr!=rob.end())
			rob_itr++;
			else
			break;
		}


}

bool add1_insert()
{
    list<r_slot>::iterator i;
    int j=0;

    if(add1_sz<add1_max)
    {
			cout<<"in add1_insert()"<<endl;
			string id=addtorob(idq.front());       //adding to rob
            if(strcmp(id.c_str(),"---"))           //rob slot is available
                {

					struct r_slot rs1;
					rs1.avail=false;
					rs1.inr=idq.front();
					rs1.id=id;
					perform_reg_renaming(rs1);
					cout<<"return from reg_renaming"<<endl;
					add1.push_back(rs1);
					add1_sz++;

					idq.pop();

					return true;

                }
               cout<<"r_slot  add1 size = "<<add1_sz<<endl;
	}
	else
	{

		for(i=add1.begin();i!=add1.end();i++)
			{

				//cout<<"in add1_insert() second half"<<endl;

				if((i->avail==true))
					{
                        string id=addtorob(idq.front());       //adding to rob
                        if(strcmp(id.c_str(),"---"))
                        {
                        cout<<"inside for in add1 insert"<<endl;
						i->avail=false;
						i->inr=idq.front();     //assigning instruction to reservation slot
					    perform_reg_renaming((*i));
						// //study register renaming and perform this function
						idq.pop();

						i->id=id;
						return true;
                        }
					}
			}
    }

	return false;
}
void add2_insert()
{
    list<r_slot>::iterator i;
    int j=0;

    if(add2_sz<add2_max)
    {
			cout<<"in add2_insert()"<<endl;
			string id=addtorob(idq.front());       //adding to rob
            if(strcmp(id.c_str(),"---"))
                {

					struct r_slot rs1;
					rs1.avail=false;
					rs1.inr=idq.front();
					rs1.id=id;
					perform_reg_renaming(rs1);
					add2.push_back(rs1);
					add2_sz++;
                    idq.pop();
                }
               cout<<"r_slot  add2 size = "<<add2_sz<<endl;
	}
	else
	{

		for(i=add2.begin();i!=add2.end();i++)
			{

				//cout<<"in add1_insert() second half"<<endl;

				if((i->avail==true))
					{
                        string id=addtorob(idq.front());       //adding to rob
                        if(strcmp(id.c_str(),"---"))
                        {
                        cout<<"inside for in add2 insert"<<endl;
						i->avail=false;
						i->inr=idq.front();     //assigning instruction to reservation slot
					    perform_reg_renaming((*i));
						// //study register renaming and perform this function
						idq.pop();

						i->id=id;

                        }
					}
			}
    }

}

void mul_insert()
{
	list<r_slot>::iterator i;
    int j=0;

    if(mul_sz<mul_max)
    {
			cout<<"in mul_insert()"<<endl;
			string id=addtorob(idq.front());       //adding to rob
            if(strcmp(id.c_str(),"---"))
                {

					struct r_slot rs1;
					rs1.avail=false;
					rs1.inr=idq.front();
					rs1.id=id;
					perform_reg_renaming(rs1);
					mul.push_back(rs1);
					mul_sz++;

					idq.pop();
                }
               cout<<"r_slot  mul size = "<<mul_sz<<endl;
	}
	else
	{

		for(i=mul.begin();i!=mul.end();i++)
			{
				if((i->avail==true))
					{
                        string id=addtorob(idq.front());       //adding to rob
                        if(strcmp(id.c_str(),"---"))
                        {
                        cout<<"inside for in mul insert"<<endl;
						i->avail=false;
						i->inr=idq.front();     //assigning instruction to reservation slot
					    perform_reg_renaming((*i));
                        idq.pop();

						i->id=id;

                        }
					}
			}
    }

}

void dson_insert()
{
    list<r_slot>::iterator i;
    int j=0;
    cout<<"allo"<<endl;
    if(dson_sz<dson_max)
    {
			cout<<"in dson_insert()"<<endl;
			string id=addtorob(idq.front());       //adding to rob
            if(strcmp(id.c_str(),"---"))
                {

					struct r_slot rs1;
					rs1.avail=false;
					rs1.inr=idq.front();
					rs1.id=id;
					perform_reg_renaming(rs1);
					dson.push_back(rs1);
					dson_sz++;
                    cout<<"dson_sz == "<<dson_sz<<endl;
					idq.pop();



                }
             //  cout<<"r_slot  dson size = "<<dson_sz<<endl;
	}
	else
	{

		for(i=dson.begin();i!=dson.end();i++)
			{

				if((i->avail==true))
					{
                        string id=addtorob(idq.front());       //adding to rob
                        if(strcmp(id.c_str(),"---"))
                        {
                        cout<<"inside for in dson insert"<<endl;
						i->avail=false;
						i->inr=idq.front();     //assigning instruction to reservation slot
					    perform_reg_renaming((*i));
						// //study register renaming and perform this function
						idq.pop();

						i->id=id;

                        }
					}
			}
    }

}

void def_insert()
{
    list<r_slot>::iterator i;
    int j=0;

    if(def_sz<def_max)
    {
			cout<<"in def_insert()"<<endl;
			string id=addtorob(idq.front());       //adding to rob
            if(strcmp(id.c_str(),"---"))
                {
                   cout<<"\nAdding to rob\n"<<endl;
					struct r_slot rs1;
					rs1.avail=false;
					rs1.inr=idq.front();
					rs1.id=id;
					perform_reg_renaming(rs1);
					def.push_back(rs1);
					def_sz++;

                    cout<<"def sz = "<<def_sz<<endl;
					idq.pop();



                }
               //cout<<"r_slot  def size = "<<dson_sz<<endl;
	}
	else
	{

		for(i=def.begin();i!=def.end();i++)
			{

				if((i->avail==true))
					{
                        string id=addtorob(idq.front());       //adding to rob
                        if(strcmp(id.c_str(),"---"))
                        {
                        cout<<"inside for in def insert"<<endl;
						i->avail=false;
						i->inr=idq.front();     //assigning instruction to reservation slot
					    perform_reg_renaming((*i));
						idq.pop();

						i->id=id;

                        }
					}
			}
    }

}
void issue()
{

        instr hinr;             //used to check the head of the queue
        string h_opc;

        hinr=idq.front();
        h_opc=hinr.opcode;


        if((!strcmp("ADD",h_opc.c_str()))||(!strcmp("ADC",h_opc.c_str()))||(!strcmp("SUB",h_opc.c_str()))||(!strcmp("MOV",h_opc.c_str()))||(!strcmp("LEA",h_opc.c_str())))
        {
			bool flag =  add1_insert();

            if(flag==false)
            {
                add2_insert();
            }
        }
      else if((!strcmp("MUL",h_opc.c_str()))||(!strcmp("IMUL",h_opc.c_str()))||(!strcmp("FMUL",h_opc.c_str())))
        {
			if(mul_sz<mul_max)
			{
				mul_insert();
			}
        }
        else if((!strcmp("DIV",h_opc.c_str()))||(!strcmp("IDIV",h_opc.c_str()))||(!strcmp("FDIV",h_opc.c_str())))
        {
			if(dson_sz<dson_max)
			{
				dson_insert();
			}
        }
        else if(!strcmp("CALL_NEAR",h_opc.c_str()))
        {

            add1_insert();
        }
        else
        {
            if(def_sz<def_max)
            {
                def_insert();
            }
        }


}

//book-keeping function used to print the contents of the reservation slot array
void print_rslotq(list<r_slot> l)
{
                list<r_slot>::iterator i;
                int j=0;
                cout<<"\n---------\n";
                for(i=l.begin();i!=l.end();i++)
                {
                    cout<<(*i).id<<" ";
                    (*i).inr.print_instr();
                    for(j=0;j<nsrc;j++)
                    {
                        cout<<"src "<<j<<" is rslot "<<(*i).reg_rename[j].name<<" ";
                    }
                    cout<<(*i).avail;
                    cout<<"-----"<<endl;
                }
}

string convertInt(int number)
{
   stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}


r_slot* add1_get_instr_rslot()
{

    list<r_slot>::iterator itr;
    int i;
    for(itr=add1.begin();itr!=add1.end();itr++)
    {
        int flag=0;
        if((*itr).avail==false)
        {
            for(i=0;i<nsrc;i++)
            {
                if((*itr).reg_rename[i].flag==false)
                {
                    //cout<<"\nhab "<<(*itr).reg_rename[i].name.c_str()<<endl;
                }
                else
                {
                    flag=1;
                    break;
                }
            }
        }
        if(flag==0 && (*itr).avail==false)
        {
            (*itr).avail=true;
            cout<<"IN THE FLAG CONDITION "<<(*itr).inr.in_addr<<endl;
            return &(*itr);
        }
    }

    return NULL;
}


r_slot* add2_get_instr_rslot()
{

    list<r_slot>::iterator itr;
    int i;
    for(itr=add2.begin();itr!=add2.end();itr++)
    {
        int flag=0;
        //cout<<"going through add2 "<<(*itr).inr.in_addr<<" with size = "<<(int)add2.size()<<endl;
        if((*itr).avail==false)
        {
            for(i=0;i<nsrc;i++)
            {
                if((*itr).reg_rename[i].flag==false)
                {

                }
                else
                {
                    flag=1;
                    break;
                }
            }
        }
        if(flag==0 && (*itr).avail==false)
        {
            (*itr).avail=true;
            cout<<"IN THE FLAG CONDITION "<<(*itr).inr.in_addr<<endl;
            return &(*itr);
        }
    }

    return NULL;
}


r_slot* mul_get_instr_rslot()
{

    list<r_slot>::iterator itr;
    int i;
    for(itr=mul.begin();itr!=mul.end();itr++)
    {
        int flag=0;
        //cout<<"going through mul "<<(*itr).inr.in_addr<<" with size = "<<(int)mul.size()<<endl;
        if((*itr).avail==false)
        {
            for(i=0;i<nsrc;i++)
            {
                if((*itr).reg_rename[i].flag==false)
                {

                }
                else
                {
                    flag=1;
                    break;
                }
            }
        }
        if(flag==0 && (*itr).avail==false)
        {
            (*itr).avail=true;
            cout<<"IN THE FLAG CONDITION "<<(*itr).inr.in_addr<<endl;
            return &(*itr);
        }
    }

    return NULL;
}

r_slot* dson_get_instr_rslot()
{

    list<r_slot>::iterator itr;
    int i;
    for(itr=dson.begin();itr!=dson.end();itr++)
    {
        int flag=0;
        //cout<<"going through dson "<<(*itr).inr.in_addr<<" with size = "<<(int)dson.size()<<endl;
        if((*itr).avail==false)
        {
            for(i=0;i<nsrc;i++)
            {
                if((*itr).reg_rename[i].flag==false)
                {

                }
                else
                {
                    flag=1;
                    break;
                }
            }
        }
        if(flag==0 && (*itr).avail==false)
        {
            (*itr).avail=true;
            cout<<"IN THE FLAG CONDITION "<<(*itr).inr.in_addr<<endl;
            return &(*itr);
        }
    }

    return NULL;
}

r_slot* def_get_instr_rslot()
{

    list<r_slot>::iterator itr;
    int i;
    //cout<<"def.size() == "<<def.size()<<endl;
    for(itr=def.begin();itr!=def.end();itr++)
    {
        int flag=0;
       // cout<<"going through def "<<(*itr).inr.in_addr<<" with size = "<<(int)def.size()<<endl;
        if((*itr).avail==false)
        {
            for(i=0;i<nsrc;i++)
            {
                 if((*itr).reg_rename[i].flag==false)
                {
                   // cout<<"\nasdsa\n";
                }
                else
                {
                    flag=1;
                    break;
                }
            }
        }
        if(flag==0 && (*itr).avail==false)
        {
            (*itr).avail=true;
            (*itr).inr.in_addr="";
            cout<<"IN THE FLAG CONDITION "<<(*itr).inr.in_addr<<endl;

            return &(*itr);
        }
    }

    return NULL;
}

/*
void erase_rslot_entry(list<r_slot>l,r_slot * r)
{
    list<r_slot>::iterator itr;

    for(itr=l.begin();itr!=l.end();itr++)
    {
        if(&(*itr)==r)
        {
            cout<<"I FOUND THE RSLOT ENTRY"<<endl;
            l.erase(itr);
            cout<<"AFTER ERASE"<<endl;
        }
    }
}*/
void execute_add1()
{

    if((clk-add1_stclk)==add1_wttime)
    {
        cout <<"somewhere in first half"<<endl;
        if(add1_ready==false)
        {
          cout<<"IN EXECUTE 1 first half"<<endl;
        //cout<<"end instr"<<endl;
        add1_ready=true;
        r_slot cdb_temp;
        cdb_temp.avail=cur_add1.avail;
        cdb_temp.id=cur_add1.id;
        cdb_temp.inr=cur_add1.inr;

         if(!strcmp(cur_add1.inr.opcode.c_str(),"CALL_NEAR"))
         {
             allow_fetch=true;  //allow fetching after branch has been evaluated
         }
            int i=0;
            for(i=0;i<nsrc;i++)
            {
                cdb_temp.reg_rename[i]=cur_add1.reg_rename[i];
            }
            cur_add1.avail=true;

            cdb.push_back(cdb_temp);
        cout<<"add1 end clock"<<clk<<endl;
        add1_stclk=0;
        return ;
        }
    }

    if(add1_ready==true)
    {
        r_slot* r1 = add1_get_instr_rslot();

        if(r1!=NULL)
        {
            cout<<"IN EXECUTE 1 second half"<<endl;

            r1->avail=true;
            cout<<"begin instr"<<endl;
            cur_add1.avail=false;
            cur_add1.id=(*r1).id;
            cur_add1.inr=r1->inr;

            int i=0;
            for(i=0;i<nsrc;i++)
            {
                cur_add1.reg_rename[i]=r1->reg_rename[i];
            }
            r1->avail=true;
            add1_ready=false;
            add1_stclk=clk;
            cout<<"add1_stclk = "<<add1_stclk<<endl;

        }
        /*else
        {
            cout<<"\nrslot == null\n";
        }*/
    }
}
void execute_add2()
{
    if((clk-add2_stclk)==add2_wttime)
    {
        if(add2_ready==false)
        {
        cout<<"IN EXECUTE 1 first half"<<endl;
        cout<<"end instr"<<endl;
        add2_ready=true;
        r_slot cdb_temp;
        cdb_temp.avail=cur_add2.avail;
        cdb_temp.id=cur_add2.id;
        cdb_temp.inr=cur_add2.inr;

            int i=0;
            for(i=0;i<nsrc;i++)
            {
                cdb_temp.reg_rename[i]=cur_add2.reg_rename[i];
            }
            cur_add2.avail=true;

            cdb.push_back(cdb_temp);
        cout<<"add1 end clock"<<clk<<endl;
        add2_stclk=0;
        return ;
        }
    }

    if(add2_ready==true)
    {
        r_slot* r1 = add2_get_instr_rslot();


        if(r1!=NULL)
        {
            cout<<"IN EXECUTE 1 second half"<<endl;

            r1->avail=true;
            cout<<"begin instr"<<endl;
            cur_add2.avail=false;
            cur_add2.id=(*r1).id;
            cur_add2.inr=r1->inr;

            int i=0;
            for(i=0;i<nsrc;i++)
            {
                cur_add2.reg_rename[i]=r1->reg_rename[i];
                //r1->reg_rename[i].ds_type="";
                //r1->reg_rename[i].name="";
                //r1->reg_rename[i].ref=' ';
            }
            r1->avail=true;
            //r1->id="";
            //r1->inr=NULL;
            add2_ready=false;
            add2_stclk=clk;
            cout<<"add2_stclk = "<<add2_stclk<<endl;

        }
    }
}
void execute_mul()
{
    if((clk-mul_stclk)==mul_wttime)
    {
        if(mul_ready==false)
        {
        cout<<"end instr"<<endl;
        mul_ready=true;
        r_slot cdb_temp;
        cdb_temp.avail=cur_mul.avail;
        cdb_temp.id=cur_mul.id;
        cdb_temp.inr=cur_mul.inr;

            int i=0;
            for(i=0;i<nsrc;i++)
            {
                cdb_temp.reg_rename[i]=cur_mul.reg_rename[i];
            }
            cur_mul.avail=true;

            cdb.push_back(cdb_temp);
        cout<<"mul end clock"<<clk<<endl;
        mul_stclk=0;
        return ;
        }
    }

    if(mul_ready==true)
    {
        //cout<<"IN EXECUTE 1 second half"<<endl;
        r_slot* r1 = mul_get_instr_rslot();


        if(r1!=NULL)
        {
         //   cout<<"IN EXECUTE 1 second half"<<endl;

            r1->avail=true;
            cout<<"begin instr"<<endl;
            cur_mul.avail=false;
            cur_mul.id=(*r1).id;
            cur_mul.inr=r1->inr;

            int i=0;
            for(i=0;i<nsrc;i++)
            {
                cur_mul.reg_rename[i]=r1->reg_rename[i];
                //r1->reg_rename[i].ds_type="";
                //r1->reg_rename[i].name="";
                //r1->reg_rename[i].ref=' ';
            }
            r1->avail=true;
            //r1->id="";
            //r1->inr=NULL;
            mul_ready=false;
            mul_stclk=clk;
            cout<<"mul_stclk = "<<mul_stclk<<endl;

        }
    }

}
void execute_dson()
{
if((clk-dson_stclk)==dson_wttime)
    {
        if(dson_ready==false)
        {
        cout<<"IN EXECUTE 1 first half"<<endl;
        cout<<"end instr"<<endl;
        dson_ready=true;
        r_slot cdb_temp;
        cdb_temp.avail=cur_dson.avail;
        cdb_temp.id=cur_dson.id;
        cdb_temp.inr=cur_dson.inr;

            int i=0;
            for(i=0;i<nsrc;i++)
            {
                cdb_temp.reg_rename[i]=cur_dson.reg_rename[i];
            }
            cur_dson.avail=true;

            cdb.push_back(cdb_temp);
        cout<<"dson end clock"<<clk<<endl;
        dson_stclk=0;
        return ;
        }
    }

    if(dson_ready==true)
    {
        //cout<<"IN EXECUTE 1 second half"<<endl;
        r_slot* r1 = dson_get_instr_rslot();


        if(r1!=NULL)
        {
            cout<<"IN EXECUTE 1 second half"<<endl;

            r1->avail=true;
            cout<<"begin instr"<<endl;
            cur_dson.avail=false;
            cur_dson.id=(*r1).id;
            cur_dson.inr=r1->inr;

            int i=0;
            for(i=0;i<nsrc;i++)
            {
                cur_dson.reg_rename[i]=r1->reg_rename[i];
                //r1->reg_rename[i].ds_type="";
                //r1->reg_rename[i].name="";
                //r1->reg_rename[i].ref=' ';
            }
            r1->avail=true;
            //r1->id="";
            //r1->inr=NULL;
            dson_ready=false;
            dson_stclk=clk;
            cout<<"dson_stclk = "<<dson_stclk<<endl;

        }
    }

}
void execute_def()
{
    //cout<<"In execute def_stclk ="<<def_stclk<<"clk = "<<clk<<endl;
if(((clk-def_stclk)%def_wttime)==0)
    {
        //cout<<"hello world"<<endl;
        if(def_ready==false)
        {
        cout<<"IN EXECUTE 1 first half"<<endl;
        cout<<"end instr"<<cur_def.id<<endl;
        def_ready=true;
        r_slot cdb_temp;
        cdb_temp.avail=cur_def.avail;
        cdb_temp.id=cur_def.id;
        cdb_temp.inr=cur_def.inr;

            int i=0;
            for(i=0;i<nsrc;i++)
            {
                cdb_temp.reg_rename[i]=cur_def.reg_rename[i];
            }
            cur_def.avail=true;

            cdb.push_back(cdb_temp);
        cout<<"def end clock"<<clk<<endl;
        def_stclk=0;
        return ;
        }
    }

    if(def_ready==true)
    {

        //cout<<"IN EXECUTE 1 second half"<<endl;
        r_slot* r1 = def_get_instr_rslot();

        if(r1!=NULL)
        {

            cout<<"IN EXECUTE 1 second half2"<<endl;
            cout<<"begin instr"<<cur_def.id<<endl;
            r1->avail=true;
            cout<<"begin instr"<<endl;
            cur_def.avail=false;
            cur_def.id=(*r1).id;
            cur_def.inr=r1->inr;

            int i=0;
            for(i=0;i<nsrc;i++)
            {
                cur_def.reg_rename[i]=r1->reg_rename[i];
                //r1->reg_rename[i].ds_type="";
                //r1->reg_rename[i].name="";
                //r1->reg_rename[i].ref=' ';
            }
            r1->avail=true;
            //r1->id="";
            //r1->inr=NULL;
            def_ready=false;
            def_stclk=clk;
           // cout<<"def_stclk = "<<def_stclk<<endl;

        }
    }

}
void execute()
{
    cout<<"In execute clk = "<<clk<<endl;
    execute_add1();
    execute_add2();
    execute_mul();
    execute_dson();
    execute_def();
}

void add1_update_resv_slots(r_slot r1)
{
    list<r_slot>::iterator itr;
    int i=0;
    for(itr=add1.begin();itr!=add1.end();itr++)
        {
            for(i=0;i<nsrc;i++)
            {

                if((*itr).reg_rename[i].name==r1.id)
                {
                   (*itr).reg_rename[i].name="";
                   (*itr).reg_rename[i].flag=false;
                }
            }
        }
}
void add2_update_resv_slots(r_slot r1)
{
    list<r_slot>::iterator itr;
    int i=0;
    for(itr=add2.begin();itr!=add2.end();itr++)
        {
            for(i=0;i<nsrc;i++)
            {

                if((*itr).reg_rename[i].name==r1.id)
                {
                   (*itr).reg_rename[i].name="";
                   (*itr).reg_rename[i].flag=false;
                }
            }
        }
}
void mul_update_resv_slots(r_slot r1)
{
    list<r_slot>::iterator itr;
    int i=0;
    for(itr=mul.begin();itr!=mul.end();itr++)
        {
            for(i=0;i<nsrc;i++)
            {

                if((*itr).reg_rename[i].name==r1.id)
                {
                   (*itr).reg_rename[i].name="";
                   (*itr).reg_rename[i].flag=false;
                }
            }
        }
}

void dson_update_resv_slots(r_slot r1)
{
    list<r_slot>::iterator itr;
    int i=0;
    for(itr=dson.begin();itr!=dson.end();itr++)
        {
            for(i=0;i<nsrc;i++)
            {

                if((*itr).reg_rename[i].name==r1.id)
                {
                   (*itr).reg_rename[i].name="";
                   (*itr).reg_rename[i].flag=false;
                }
            }
        }
}

void def_update_resv_slots(r_slot r1)
{
    list<r_slot>::iterator itr;
    int i=0;
    for(itr=def.begin();itr!=def.end();itr++)
        {
            for(i=0;i<nsrc;i++)
            {

                if((*itr).reg_rename[i].name==r1.id)
                {
                   (*itr).reg_rename[i].name="";
                   (*itr).reg_rename[i].flag=false;
                }
            }
        }
}


void write_back()
{


    while(!cdb.empty())
    {
        r_slot r1=cdb.front();
        add1_update_resv_slots(r1);
        add2_update_resv_slots(r1);
        mul_update_resv_slots(r1);
        dson_update_resv_slots(r1);
        def_update_resv_slots(r1);

        rob[r1.id].ready=true;

        //update rob to commit ready..

        cout<<"instr id in cdb = "<<cdb.front().id<<endl;
        cdb.pop_front();
    }

}
void commit()                                            //commit instructions from the head of the rob in-order
{
    bool ret_val;
    map<string,rob_entry>::iterator itr;
   // map<string,rob_entry>::iterator str_itr;   //used to send store instructions to store buffer

    if(rob.size()>0)
    {
    itr=rob.begin();
    ret_val=(*itr).second.ready;
    while(ret_val==true)
    {

        instr str_inr = (*itr).second.inr;
        if(!strcmp(str_inr.opcode.c_str(),"MOV"))
        {
           int i =0;

           for(i=0;i<ndest;i++)
           {
               if(str_inr.d[i].ref=='M')
               {
                   str_buf.push((*itr).second);
                   break;

               }
           }
        }

        rob.erase(itr);
        if(rob.size()==0)
        {
      //      cout<<"hello rob = 0"<<endl;
            break;
        }
        itr=rob.begin();
        ret_val=(*itr).second.ready;
    }
    }
}

void perform_store()                //complete  a store if 10 cycles have passed
{
    if(((clk-str_stclk)==str_buf_wttime)&&(str_buf.size()>0)&&(str_buf_ready==false))
    {
        str_buf.pop();
        str_buf_ready=true;
    }
    if((str_buf.size()>0)&&(str_buf_ready==true))
    {
        str_buf_ready=false;
        str_stclk=clk;
    }
}
void cycle()
{

    //cout<<"in cycle rem = "<<rem<<endl;
    if((clk%start_time==0) && (ifq_size < ifq_size_max))
        {
               cout<<"BEGINNING FETCH in clk = "<<clk<<endl;
               fetch_instr();
                                                         //fetch an instruction every 10 cycles
		//char ch;
              	//ch = getchar();

        }

    if(clk>=(start_time+1))
    decode1();

    if(clk>=(start_time+2))
    decode2();

    if(clk>=(start_time+3) && idq.size()>0)
    {
		//cout<<"in issue"<<endl;
		issue();
	}

	if(clk>=(start_time+4))
	{
	    execute();
	}

    if(clk>=(start_time+5))
    {
        write_back();
    }
    if(clk>=(start_time+6))
    {
        commit();
    }
    if(clk>=(start_time+7))
    {
        perform_store();
    }
    clk++;

    //execute();
    //clk+=2;
    //execute();
    //clk+=con_clk;

}

int main(int argc,char *argv[])
{
    ipfile.open("newip_3");

    ifq_size_max=atoi(argv[1]);
    add1_max=atoi(argv[2]);
    add2_max=atoi(argv[3]);
    mul_max=atoi(argv[4]);
    dson_max=atoi(argv[5]);
    def_max=atoi(argv[6]);
    rob_sz_max=atoi(argv[7]);

    while(endflag!=1)
    {
        cycle();
    }

    if(endflag==1)  //once eof is reached execute remaining instructions in pipeline
    {
        while((!ifq.empty())&&(!idq.empty())&&(!rob.empty())&&(!str_buf.empty()))
        cycle();
    }

    cout<<"\n"<<(clk+rem_clk);
    return 0;
}
