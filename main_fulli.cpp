#include <stdio.h>
#include "pin.H"
#include<string>
#include<fstream>
#define SKIP_INST_LIM 50000

FILE * trace;
int flag1=0;
long inst_count=0;
//REG rtemp;
int tot_pred;
int cor_pred;

VOID printip(VOID *ip)
 { 
	fprintf(trace, " instr_addr = %p \n---------------\n ", ip);
	flag1=1;
	return ;
 }


VOID RecordMemRead(VOID * ip, VOID * addr)
{
    fprintf(trace,"inst_addr =  %p SOURCE %p \n---------------\n",ip,addr);
}



VOID RecordMemWrite(VOID * ip, VOID * addr)
{
    fprintf(trace,"inst_addr = %p DEST %p \n---------------\n",ip,addr);
        
}

VOID print_opcode(OPCODE ins_opc1)
{
	
fprintf(trace,"%s \n---------------\n ",OPCODE_StringShort(ins_opc1).c_str());

}

VOID print_source_regname(REG rtemp)
{
	
fprintf(trace," SOURCE r%s \n---------------\n ",REG_StringShort(rtemp).c_str());
		
}

VOID print_dest_regname(REG rtemp)
{
	
fprintf(trace," DEST r%s \n---------------\n ",REG_StringShort(rtemp).c_str());
		
}


VOID docount() 
{

 inst_count++; 

}

VOID RecordImplicit(REG rtemp)
{
	
fprintf(trace," \n---Implicit Memory Operand---\n ");
		
}

VOID Instruction(INS ins, VOID *v)
{
    
     
    int i=0,j=0,no_rops=0,no_wops=0;
    int memOp=0,memOperands=0;
    //int k=0,genOperand=0;	
	
	INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)docount,IARG_END);
       
      if(inst_count>=SKIP_INST_LIM && inst_count <= 10000000)
      {
    
    		INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)printip, IARG_INST_PTR, IARG_END); //print instruction address
    		
		INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)print_opcode,IARG_ADDRINT,INS_Opcode(ins),IARG_END);  //print instruction opcode
		
	
    		no_rops = INS_MaxNumRRegs(ins) ;  //number of register read operands
 		no_wops = INS_MaxNumWRegs(ins) ;  //number or register write operands
    		memOperands = INS_MemoryOperandCount(ins);     //number of memory operands
		//genOperand=INS_OperandCount();
		for(i=0;i<no_rops;i++)
		{
			INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)print_source_regname,IARG_ADDRINT,INS_RegR(ins,i),IARG_END);
				
		}
		for(j=0;j<no_wops;j++)
		{
			INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)print_dest_regname,IARG_ADDRINT,INS_RegW(ins,j),IARG_END);
				
		}
	    for (memOp=0;memOp<memOperands;memOp++)
		    {    	
			
			//recording the memory accesses
			
				if (INS_MemoryOperandIsRead(ins, memOp))
				{
				   // INS_IsStackRead	( 	const INS 	ins	 ) 	
 					if(INS_OperandIsImplicit(ins,memOp))      //checking if memory access is implicit...
					{
						INS_InsertPredicatedCall(ins, IPOINT_BEFORE,(AFUNPTR)RecordImplicit,IARG_END);	
					}
					 	
				    INS_InsertPredicatedCall(
					ins, IPOINT_BEFORE, (AFUNPTR)RecordMemRead,IARG_INST_PTR,IARG_MEMORYOP_EA, memOp,IARG_END);
				}
				// Note that in some architectures a single memory operand can be 
				// both read and written (for instance incl (%eax) on IA-32)
				// In that case we instrument it once for read and once for write.
				if (INS_MemoryOperandIsWritten(ins, memOp))
				{
					if(INS_OperandIsImplicit(ins,memOp))       //checking if memory access is implicit...
					{
						INS_InsertPredicatedCall(ins, IPOINT_BEFORE,(AFUNPTR)RecordImplicit,IARG_END);	
					}
				    INS_InsertPredicatedCall(ins, IPOINT_BEFORE,(AFUNPTR)RecordMemWrite,IARG_INST_PTR,IARG_MEMORYOP_EA, 					memOp,IARG_END);

				}
			
		    }
	     
	     
     fprintf(trace, "\n"); 	
   }	
}

// This function is called when the application exits
VOID Fini(INT32 code, VOID *v)
{
    //fprintf(trace," no_memops");
    fprintf(trace, "abcds #eof\n");
    fclose(trace);
}

/* ===================================================================== */
/* Print Help Message                                                    */
/* ===================================================================== */

INT32 Usage()
{
    PIN_ERROR("This Pintool prints the IPs of every instruction executed\n" 
              + KNOB_BASE::StringKnobSummary() + "\n");
    return -1;
}

/* ===================================================================== */
/* Main                                                                  */
/* ===================================================================== */

int main(int argc, char * argv[])
{
    trace = fopen("main_after.out", "w");
    
    // Initialize pin
    if (PIN_Init(argc, argv)) return Usage();

    // Register Instruction to be called to instrument instructions
    INS_AddInstrumentFunction(Instruction, 0);

    // Register Fini to be called when the application exits
    PIN_AddFiniFunction(Fini, 0);
    
    // Start the program, never returns
    PIN_StartProgram();
    
    return 0;
}
