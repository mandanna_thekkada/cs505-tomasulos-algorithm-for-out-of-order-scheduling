#include<iostream>
#include<cstdlib>
#include<queue>
#include<fstream>
#include<cstring>
#include<map>
#include<list>
#include<sstream>
#define nsrc 10
#define ndest 10
#define start_time 10
using namespace std;

struct opr
{
    string ds_type;                   //destination or source operand
    string name;                        //value of operand
    char ref;                         //memory or register reference
    bool flag;
};

class instr
{
    public:
    string in_addr;
    string opcode;
    opr d[ndest];
    opr s[nsrc];
    bool sflg[nsrc];
    bool dflg[ndest];
    int sr_ct;                      //count of source operands
    int d_ct;                       //count of destination operands

    instr();
    instr(string str_instr);
    void print_instr();

};

struct register_entry
{
    string instr_tag;
    bool state;
};

struct mem
{
    string addr;
    int value;
};

struct r_slot
{
    instr inr;
    string id;
    opr reg_rename[nsrc];  //holds the renamed register names
    bool avail;            //status variable to denote availability of reservation slot
};

struct rob_entry
{
    instr inr;
    bool ready;            //ready bit denotes whether the instruction is ready to commit or not
};

typedef struct opr opr;
typedef struct r_slot r_slot;
int add1_max=5,add1_sz,add2_max=5,add2_sz,mul_max=5,mul_sz,dson_max=5,dson_sz,def_max=5,def_sz;   //size variables to maintain reservation slots list size
int idq12_size_max=2;                   //maximum size of intermediate queue
int idq12_size=0;
int ifq_size=0,ifq_size_max;            //size of instruction fetch queue

int endflag=0;                          //flag to denote end of program

int clk=0;                              //clock variable
string line;
ifstream ipfile;                        //file handler for trace file
int last_index=0;                       //index of next inserted element
int first_index=0;                      //index of first instruction that is going to commit
map<string,rob_entry>rob;               //re-order buffer
map<string,register_entry> reg_file;    //register file
queue<string>ifq;                       //instruction fetch queue
queue<instr>idq12;                      //intermediate queue between decode stage 1 and decode stage 2
queue<instr>idq;                        //instruction decode queue

queue<rob_entry>str_buf;


int rob_sz_max=0;
int rob_sz;

list<r_slot>add1;                 //reservation slots list for each functional unit
list<r_slot>add2;
list<r_slot>mul;
list<r_slot>dson;
list<r_slot>def;
list<r_slot>cdb;                   //central data buffer list

bool add1_insert();               //functions to add to respective reservation stations lists
void add2_insert();
void mul_insert();
void dson_insert();
void def_insert();

void cycle();
void fetch_instr();
void decode1();
void decode2();
void issue();
void execute();
void write_back();                          //used to write back data to the cdb and update the required rob and reservation slot entries
void commit();                              //used to perform in-order commit from the rob
void perform_store();                     //used to finish a store from the store buffer

void perform_reg_renaming(r_slot rs);
void print_register_file();
string addtorob(instr i);
string convertInt(int i);
string incString(string i);
void print_rob();

void execute();
void execute_add1();
void execute_add2();
void execute_dson();
void execute_mul();
void execute_def();

//void erase_rslot_entry(list<r_slot>l,r_slot * r);


int add1_stclk;                  //used to denote the clk cycle when the current execution unit began it's current round of execution
int add2_stclk;
int mul_stclk;
int dson_stclk;
int def_stclk;
int str_stclk;                   //for store buffer

r_slot* add1_get_instr_rslot();     //used to obtain a reservation slot that has an instruction ready to execute
r_slot* add2_get_instr_rslot();
r_slot* mul_get_instr_rslot();
r_slot* dson_get_instr_rslot();
r_slot* def_get_instr_rslot();


r_slot cur_add1;                   //used to hold the currently executing instruction in the functional unit
r_slot cur_add2;
r_slot cur_mul;
r_slot cur_dson;
r_slot cur_def;

bool add1_ready=true;
bool add2_ready=true;
bool mul_ready=true;
bool dson_ready=true;
bool def_ready=true;
bool str_buf_ready=true;

int add1_wttime=2,add2_wttime=2,mul_wttime=4,dson_wttime=12,def_wttime=1;//initialising time required for an instruction to execute and exit the FU
int str_buf_wttime=10;


void add1_update_resv_slots(r_slot r1); //these functions are used to update the reservation slots when the results are broadcast on the cdb
void add2_update_resv_slots(r_slot r1);
void mul_update_resv_slots(r_slot r1);
void dson_update_resv_slots(r_slot r1);
void def_update_resv_slots(r_slot r1);

int rem_clk=0;



//flags to perform staged exit..after reaching eof , remaining instructions in pipeline should complete execution
int add1_endflag=0;
int add2_endflag=0;
int mul_endflag=0;
int dson_endflag=0;
int def_endflag=0;


bool allow_fetch=true; // used to stop further fetching once a branch is detected in ID stage
